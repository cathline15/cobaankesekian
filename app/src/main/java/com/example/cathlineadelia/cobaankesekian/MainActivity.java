package com.example.cathlineadelia.cobaankesekian;


import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    EditText username, password;
    Button btnLogin;
    String usernameKey, passwordKey;
    private ProgressDialog pDialog;
    TextView register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        pDialog = new ProgressDialog(MainActivity.this);
        register = (TextView) findViewById(R.id.tvRegisterHere);
        register.setOnClickListener(this);

    }



    @Override
    public void onClick(View view) {
        if (view == btnLogin) {
            btnLogin();
        }
        if (view == register){
            Intent registerIntent = new Intent(MainActivity.this, RegisterActivity.class);
            MainActivity.this.startActivity(registerIntent);
        }
    }

    private void btnLogin(){
       // Beranda();
        //getting values from edit texts
        usernameKey = username.getText().toString().trim();
        passwordKey = password.getText().toString().trim();
        if(username.equals("") || password.equals("")){
            //Beranda();
            Toast.makeText(getApplicationContext(), "Username atau password Tidak Boleh Kosong", Toast.LENGTH_LONG).show();
            return;
        }

        pDialog.setMessage("Login Process...");
        showDialog();
        //Creating a string request
        StringRequest stringRequest = new StringRequest(Request.Method.POST, config.LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //if we are getting success from server
                        if (response.contains(config.LOGIN_SUCCESS)) {
                            hideDialog();
                            Beranda();
                            //Toast.makeText(m, "Login Sukses", Toast.LENGHT_LONG).show();
                        } else {
                            hideDialog();
                            //Displaying an error message on toast
                            Toast.makeText(getApplicationContext(), "Invalid username or password", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //you can handle error here if you want
                        hideDialog();
                        Toast.makeText(getApplicationContext(), "The Server Unreachable", Toast.LENGTH_LONG).show();
                    }
                }){
            protected Map<String, String> getParams() throws AuthFailureError{
                Map<String,String> params = new HashMap<>();
                //adding parameters to request
                params.put(config.KEY_EMAIL, usernameKey);
                params.put(config.KEY_PASSWORD, passwordKey);

                //returning parametes
                return params;
            }

        };

        //adding the string request to the queue
        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }

    private void Beranda(){
        Intent intent = new Intent(MainActivity.this, Welcome.class);
        intent.putExtra(config.KEY_EMAIL, usernameKey);
        startActivity(intent);
    }
    private void showDialog(){
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog(){
        if(pDialog.isShowing())
            pDialog.dismiss();
    }
}


