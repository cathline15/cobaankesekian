package com.example.cathlineadelia.cobaankesekian;

import com.android.volley.toolbox.StringRequest;

/**
 * Created by Cathline Adelia on 19/12/2017.
 */

public class config {
    public static final String LOGIN_URL ="http://192.168.43.175/general%20checking/login.php";
    public static final String KEY_EMAIL ="usernameKey";
    public static final String KEY_PASSWORD ="passwordKey";
    public static final String LOGIN_SUCCESS ="success";
    public static final String KEY_NAME ="nameKey";
    public static final String KEY_ALAMAT ="alamatKey";
    public static final String KEY_NOHP = "nohpKey";
    public static final String KEY_JK = "jkKey";
    public static final String REGISTER_URL ="http://192.168.43.175/general%20checking/register.php";
    public static final String REGISTER_SUCCESS  = "Success";
}
