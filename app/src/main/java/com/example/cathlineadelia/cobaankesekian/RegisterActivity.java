package com.example.cathlineadelia.cobaankesekian;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etName, etUsername, etPassword, etAlamat, etNohp;
    Button btnRegister;
    String nameKey, usernameKey, passwordKey, alamatKey, nohpKey, jkKey="";

    private ProgressDialog pDialog;
    private RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        bindView();
        initRadioGroup();

        btnRegister.setOnClickListener(this);
        pDialog = new ProgressDialog(RegisterActivity.this);
    }

    private void bindView() {
        etName = findViewById(R.id.etName);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        etAlamat = findViewById(R.id.etAlamat);
        etNohp = findViewById(R.id.etNohp);
        btnRegister = findViewById(R.id.btnRegister);
        radioGroup = findViewById(R.id.radioGroupButton);
    }

    private void initRadioGroup() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int id = radioGroup.getCheckedRadioButtonId();
                switch (id) {
                    case R.id.radioButtonPria: {
                        jkKey = "Pria";
                        break;
                    }
                    case R.id.radioButtonWanita: {
                        jkKey = "Wanita";
                        break;
                    }
                    default: {
                        jkKey = "";
                        break;
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == btnRegister) {
            btnRegister();
        }
    }

    public void btnRegister() {
        nameKey = etName.getText().toString().trim();
        usernameKey = etUsername.getText().toString().trim();
        passwordKey = etPassword.getText().toString().trim();
        alamatKey = etAlamat.getText().toString().trim();
        nohpKey = etNohp.getText().toString().trim();

        if (etName.equals("") || etUsername.equals("") || etPassword.equals("") || etAlamat.equals("") || etNohp.equals("")) {
            return;
        }

        pDialog.setMessage("Register Process");
        showDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, config.REGISTER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains(config.REGISTER_SUCCESS)) {
                    hideDialog();
                    Formlogin();
                } else {
                    hideDialog();
                    Toast.makeText(getApplicationContext(), "Invalid ", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show ();
            }
        }) {
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(config.KEY_EMAIL, usernameKey);
                params.put(config.KEY_PASSWORD, passwordKey);
                params.put(config.KEY_NAME, nameKey);
                params.put(config.KEY_ALAMAT, alamatKey);
                params.put(config.KEY_NOHP, nohpKey);
                params.put(config.KEY_JK, jkKey);



                return params;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }

    private void Formlogin(){
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private void showDialog(){
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog(){
        if(pDialog.isShowing())
            pDialog.dismiss();
    }
}
