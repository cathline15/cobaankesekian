package com.example.cathlineadelia.cobaankesekian;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    Button btnExit;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        // Inflate the layout for this fragment

        btnExit = view.findViewById(R.id.btnExit);
        initOnClick();

        return view;
    }

    private void initOnClick() {
                btnExit.setOnClickListener(new View.OnClickListener() {
      @Override
            public void onClick(View v) {
                //perintah untuk mengakhiri aplikasi
                getActivity().finish();
            }
                });
    }

}
