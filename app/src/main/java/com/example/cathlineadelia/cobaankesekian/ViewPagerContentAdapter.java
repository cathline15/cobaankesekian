package com.example.cathlineadelia.cobaankesekian;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yudha on 12/26/2017.
 */

public class ViewPagerContentAdapter extends FragmentPagerAdapter {

    private List<Fragment> list = new ArrayList<>();

    public ViewPagerContentAdapter(FragmentManager fm, List<Fragment> list) {
        super(fm);
        this.list = list;
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
