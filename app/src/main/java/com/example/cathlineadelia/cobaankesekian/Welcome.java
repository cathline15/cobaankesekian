package com.example.cathlineadelia.cobaankesekian;

/**
 * Created by Cathline Adelia on 17/12/2017.
 */

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class Welcome extends AppCompatActivity {
//    Button btnExit;

    private ViewPager viewPagerContent;
    private BottomNavigationView bottomNavigationView;

    private List<Fragment> listFragment = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        bindView();
        initBottomNavView();

        initViewPager();
    }

    private void bindView() {
//        btnExit = findViewById(R.id.btnExit);
        viewPagerContent = findViewById(R.id.viewPagerContent);
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
    }

    private void initOnClick() {
        //        btnExit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //perintah untuk mengakhiri aplikasi
//                finish();
//            }
//        });
    }

    private void initBottomNavView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectBottomId(item.getItemId());
                return true;
            }
        });
    }

    private void addViewPagerContent() {
        listFragment.add(new HomeFragment());
        listFragment.add(new TinggiFragment());
        listFragment.add(new BeratBadanFragment());
        listFragment.add(new TekananDarahFragment());
        listFragment.add(new BMIFragment());
    }

    private void selectBottomId(int id) {
        switch (id) {
            case R.id.home: {
                viewPagerContent.setCurrentItem(0);
                break;
            }
            case R.id.tinggiBadan: {
                viewPagerContent.setCurrentItem(1);
                break;
            }
            case R.id.beratBadan: {
                viewPagerContent.setCurrentItem(2);
                break;
            }
            case R.id.tekananDarah: {
                viewPagerContent.setCurrentItem(3);
                break;
            }
            case R.id.BMI: {
                viewPagerContent.setCurrentItem(4);
                break;
            }
            default: break;
        }
    }

    private void initViewPager() {
        addViewPagerContent();

        ViewPagerContentAdapter adapter = new ViewPagerContentAdapter(getSupportFragmentManager(), listFragment);
        viewPagerContent.setAdapter(adapter);
        viewPagerContent.setCurrentItem(0);
    }
}
